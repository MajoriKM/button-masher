﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace ButtonMasher
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        int displaywidth;
        int displayheight;

        Rectangle currentSquare;
        int playerScore = 0;
        int size = 25;
        SpriteFont mainfont;
        //loaded game assetts
        Texture2D ButtonImg;
        SoundEffect clickSFX;
        SoundEffect buzzerSFX;
        SoundEffect gameoverSFX;
        SoundEffect musicSFX;
        Song music;
        Vector2 screenCentre;
        Rectangle buttonBB;

        float timeRemain = 0f;
        float timeLimit = 5f;

        bool playing = false;
        //game state
        MouseState previousState;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            displaywidth = graphics.GraphicsDevice.Viewport.Width;
            displayheight = graphics.GraphicsDevice.Viewport.Height;

            screenCentre = new Vector2(displaywidth / 2, displayheight / 2);
            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ButtonImg = Content.Load<Texture2D>("graphics/button");
            buttonBB = new Rectangle(displaywidth / 2 - ButtonImg.Width / 2, displayheight / 2 - ButtonImg.Height / 2, ButtonImg.Width, ButtonImg.Height);

            mainfont = Content.Load<SpriteFont>("mainSpriteFont");
            music = Content.Load<Song>("audio/music");
            gameoverSFX = Content.Load<SoundEffect>("audio/gameover");
            clickSFX = Content.Load<SoundEffect>("audio/buttonClick");
            buzzerSFX = Content.Load<SoundEffect>("audio/Buzzer");

            MediaPlayer.Play(music);
            MediaPlayer.IsRepeating = true;
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            MouseState mouse = Mouse.GetState();

            //currentSquare = new Rectangle( Window.ClientBounds.Width - size, Window.ClientBounds.Height - size, size, size);
            /*if (mouse.LeftButton == ButtonState.Pressed && currentSquare.Contains(mouse.X, mouse.Y))
            {
                
                playerScore++;
            }*/
            if (mouse.LeftButton == ButtonState.Pressed && previousState.LeftButton == ButtonState.Released && buttonBB.Contains(mouse.X, mouse.Y))
            {
                clickSFX.Play();
                if (playing)
                    playerScore++;
                else
                {
                    playing = true;

                    //set time remaining to full time limit
                    timeRemain = timeLimit;
                    playerScore = 0;
                }

            }
            //

            else if (mouse.LeftButton == ButtonState.Pressed && previousState.LeftButton == ButtonState.Released &! buttonBB.Contains(mouse.X, mouse.Y))
            {
                buzzerSFX.Play();

            }
            


                if (playing ==true)
                {
                    //update time remaining
                    //subtract the time passed this frame from our time remaining
                    timeRemain -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                    if(timeRemain <= 0)
                    {
                    playing = false;
                    timeRemain = 0;
                    gameoverSFX.Play();
                }
                }
            previousState = mouse;


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            spriteBatch.Draw(ButtonImg, new Rectangle(displaywidth/2, displayheight/2, ButtonImg.Width, ButtonImg.Height), null, Color.Green, 0.0f, new Vector2(ButtonImg.Width/2, ButtonImg.Height/2), SpriteEffects.None, 0 );

            Vector2 titlesize = mainfont.MeasureString("Button Masher");
            spriteBatch.DrawString(mainfont,
                "Button Masher", 
                screenCentre - new Vector2(20, 220) - titlesize / 2, 
                Color.White,
                0,
                new Vector2(0),
                1.5f,
                SpriteEffects.None,
                0);

            Vector2 authorsize = mainfont.MeasureString("By: Kieron Mead");
            spriteBatch.DrawString(mainfont,
                "By: Kieron Mead",
                screenCentre - new Vector2(-340, -220) - authorsize / 2,
                Color.White);

            string promptstrng = "Click the button to start!";
            if (playing == true)
                promptstrng = "Mash the button!";
            Vector2 promptsize = mainfont.MeasureString("Click button to start");

            spriteBatch.DrawString(mainfont,
                promptstrng,
                screenCentre - new Vector2(0, 45) - promptsize / 2,
                Color.White);

            spriteBatch.DrawString(mainfont, "Score: " + playerScore.ToString(), new Vector2(10, 10), Color.White);

            spriteBatch.DrawString(mainfont, "Time: " + timeRemain.ToString(), new Vector2(10, 30), Color.White);
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
